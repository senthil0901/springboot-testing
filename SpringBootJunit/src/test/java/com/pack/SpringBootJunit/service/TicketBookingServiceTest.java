package com.pack.SpringBootJunit.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.pack.SpringBootJunit.dao.TicketBookingDao;
import com.pack.SpringBootJunit.model.Ticket;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketBookingServiceTest {
	
	@Autowired
	private TicketBookingService ticketBookingService;
	
	@MockBean
	private TicketBookingDao ticketBookingDao;
	
	@Test
	//@Ignore
	public void testCreateTicket(){
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		Date dob=null;
		try {
			dob=sdf.parse("29/04/2009");
		}
		catch(ParseException e) {
			System.out.println(e);
		}
		Ticket ticket = new Ticket(5,"senthil",dob,"Chennai","Tirupati","senthil@gmail.com");
		
	    Mockito.when(ticketBookingDao.save(ticket)).thenReturn(ticket);    
	    assertThat(ticketBookingService.createTicket(ticket)).isEqualTo(ticket);
	}

	@Test
	public void testGetTicketById(){
		Ticket ticket=null;
	    Mockito.when(ticketBookingDao.findOne(2)).thenReturn(ticket);
	    assertThat(ticketBookingService.getTicketById(1)).isEqualTo(ticket);
	}
	
	@Test
	public void testGetAllBookedTickets(){
		List<Ticket> ticketList=null;
		Mockito.when(ticketBookingDao.findAll()).thenReturn(ticketList);		
		assertThat(ticketBookingService.getAllBookedTickets()).isEqualTo(ticketList);
	}
	
	@Test
	public void testDeleteTicket(){
		Ticket ticket=null; 
	    Mockito.when(ticketBookingDao.findOne(2)).thenReturn(ticket);
	    Mockito.when(ticketBookingDao.exists(ticket.getTicketId())).thenReturn(false);
	    assertFalse(ticketBookingDao.exists(ticket.getTicketId()));
	}
	
	
	@Test
	public void testUpdateTicket(){
		Ticket ticket = new Ticket();
		ticket.setTicketId(1);
		ticket.setPassengerName("Martin Bingel");
		ticket.setSourceStation("Kolkata");
		ticket.setDestStation("Delhi");
		ticket.setBookingDate(new Date());
		ticket.setEmail("martin.s2017@gmail.com");
		
		Mockito.when(ticketBookingDao.findOne(1)).thenReturn(ticket);
		
		ticket.setEmail("martin.s2000@gmail.com");
		Mockito.when(ticketBookingDao.save(ticket)).thenReturn(ticket);
		
		assertThat(ticketBookingService.updateTicket(1, "martin.s2017@gmail.com")).isEqualTo(ticket);
		
	}
	
}
