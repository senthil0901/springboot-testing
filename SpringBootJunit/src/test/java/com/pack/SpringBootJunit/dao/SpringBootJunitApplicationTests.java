package com.pack.SpringBootJunit.dao;


import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.pack.SpringBootJunit.dao.TicketBookingDao;
import com.pack.SpringBootJunit.model.Ticket;



@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootJunitApplicationTests {

	@Autowired
	private TicketBookingDao ticketDao;
	
	@Test
	public void testSaveTicket(){
		SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
		Date dob=null;
		try {
			dob=sdf.parse("29/04/2009");
		}
		catch(ParseException e) {
			System.out.println(e);
		}
		Ticket ticket = new Ticket(5,"senthil",dob,"Chennai","Tirupati","senthil@gmail.com");
		Ticket savedInDb = ticketDao.save(ticket);
		Ticket getFromDb = ticketDao.findOne(savedInDb.getTicketId());
		//assertEquals(savedInDb.getTicketId(),getFromDb.getTicketId());
		assertThat(getFromDb).isEqualTo(savedInDb);
	}
	
	@Test
	public void testGetAllBookedTickets(){
		Iterable<Ticket> allTicketsFromDb = ticketDao.findAll();
		List<Ticket> ticketList = new ArrayList<>();
		
		for (Ticket ticket : allTicketsFromDb) {
			ticketList.add(ticket);
		}
		assertThat(ticketList.size()).isEqualTo(4);
	}
	
	@Test
	public void testFindByEmail(){
		Ticket getFromDb = ticketDao.findByEmail("senthil@gmail.com");
		assertThat(getFromDb.getPassengerName()).isEqualTo("senthil");
	}
	
	@Test
	public void testDeleteTicketById(){
		ticketDao.delete(1);
		Iterable<Ticket> allTicketsFromDb = ticketDao.findAll();
		List<Ticket> ticketList = new ArrayList<>();
		
		for (Ticket ticket : allTicketsFromDb) {
			ticketList.add(ticket);
		}
		assertThat(ticketList.size()).isEqualTo(3);
	}
	
	@Test
	public void testUpdateTicket(){
		Ticket getFromDb = ticketDao.findByEmail("senthil@gmail.com");
		//update Email Address
		getFromDb.setEmail("senthil@yahoo.com");
		ticketDao.save(getFromDb);
		
		assertThat(getFromDb.getEmail()).isEqualTo("senthil@yahoo.com");
	}

}
