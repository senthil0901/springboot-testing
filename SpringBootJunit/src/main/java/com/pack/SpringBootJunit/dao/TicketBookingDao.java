package com.pack.SpringBootJunit.dao;

import org.springframework.data.repository.CrudRepository;

import com.pack.SpringBootJunit.model.Ticket;

public interface TicketBookingDao extends CrudRepository<Ticket, Integer>{
	Ticket findByEmail(String email);
}