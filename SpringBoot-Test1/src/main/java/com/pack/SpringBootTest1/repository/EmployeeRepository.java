package com.pack.SpringBootTest1.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pack.SpringBootTest1.model.Employee;



public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
