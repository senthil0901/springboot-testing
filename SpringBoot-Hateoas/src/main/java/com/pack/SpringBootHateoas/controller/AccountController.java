package com.pack.SpringBootHateoas.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pack.SpringBootHateoas.model.Account;
import com.pack.SpringBootHateoas.model.Transfer;
import com.pack.SpringBootHateoas.response.SuccessResponse;
import com.pack.SpringBootHateoas.service.AccountService;

@RestController
@RequestMapping("/api/v1")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/account/{id}")
    public ResponseEntity<?> getAccountDetail(@PathVariable Long id)
    {
    	//creates ResponseEntity of type either SuccessResponse or ErrorResponse
        ResponseEntity<?> responseEntity =  accountService.findOne(id);
        //create a success object if response is success
        SuccessResponse successResponse = (SuccessResponse) responseEntity.getBody();
        //retrieve account object from successresponse
        Account account = (Account) successResponse.getObject();

        //create EntityModel which will be resource on which we can create link
        EntityModel<Account> resource = EntityModel.of(account);
        List<String> allowedActions = allowedActions(account);
        allowedActions.stream().forEach(action ->{
            if(action.equalsIgnoreCase("DEPOSIT"))
            {
                resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).deposit(new Account())).withRel("deposit"));
            }
            if(action.equalsIgnoreCase("TRANSFER"))
            {
                resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).transfer(new Transfer())).withRel("transfer"));
            }
            if(action.equalsIgnoreCase("WITHDRAW"))
            {
                resource.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).withdraw(new Account())).withRel("withdraw"));
            }
        });
        return new ResponseEntity<>(resource, HttpStatus.OK);

    }
    @PostMapping("/create")
    public ResponseEntity<?> createAccount(@RequestBody Account account)
    {
        return accountService.createAccount(account);
    }

    @PutMapping("/deposit")
    public ResponseEntity<?> deposit(@RequestBody Account account)
    {
        return accountService.depositToAccount(account);
    }

    @PutMapping("/withdraw")
    public ResponseEntity<?> withdraw(@RequestBody Account account)
    {
        return accountService.withdrawFromAccount(account);
    }
    @PostMapping("/transfer")
    public ResponseEntity<?> transfer(@RequestBody Transfer transfer)
    {
        return accountService.transfer(transfer);
    }

    protected List<String> allowedActions(Account account){

        List<String> actions = new ArrayList<>();
        if(account.getAmount() <= 0)
        {
            actions.add("DEPOSIT");
        }
        else
        {
            actions.add("DEPOSIT");
            actions.add("TRANSFER");
            actions.add("WITHDRAW");
        }

        return actions;
    }
}
