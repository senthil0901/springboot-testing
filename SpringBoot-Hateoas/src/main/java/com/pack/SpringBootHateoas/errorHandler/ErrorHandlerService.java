package com.pack.SpringBootHateoas.errorHandler;

import org.springframework.stereotype.Service;

import com.pack.SpringBootHateoas.response.ErrorResponse;

@Service
public class ErrorHandlerService{

    public ErrorResponse insufficientFunds(String message, int statusCode, Object object)
    {
        ErrorResponse errorResponse = new ErrorResponse(message,object,statusCode);
        return errorResponse;
    }

    public ErrorResponse inValidAccountNumber(String message,int statusCode,Object object)
    {
        ErrorResponse errorResponse = new ErrorResponse(message,object,statusCode);
        return errorResponse;
    }

    public ErrorResponse accountNotActive(String message,int statusCode,Object object)
    {
        ErrorResponse errorResponse = new ErrorResponse(message,object,statusCode);
        return errorResponse;
    }

    public ErrorResponse invalidToAccount(String message,int statusCode,Object object)
    {
        ErrorResponse errorResponse = new ErrorResponse(message,object,statusCode);
        return errorResponse;
    }
    public ErrorResponse invalidFromAccount(String message,int statusCode,Object object)
    {
        ErrorResponse errorResponse = new ErrorResponse(message,object,statusCode);
        return errorResponse;
    }



}
