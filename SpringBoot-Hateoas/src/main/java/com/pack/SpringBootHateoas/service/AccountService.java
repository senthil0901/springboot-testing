package com.pack.SpringBootHateoas.service;

import org.springframework.http.ResponseEntity;

import com.pack.SpringBootHateoas.model.Account;
import com.pack.SpringBootHateoas.model.Transfer;

public interface AccountService {

    public ResponseEntity<?> findOne(Long accountNumber);
    public ResponseEntity<?> depositToAccount(Account account);
    public ResponseEntity<?> withdrawFromAccount(Account account);
    public ResponseEntity<?> createAccount(Account account);
    public ResponseEntity<?> transfer(Transfer transfer);

}