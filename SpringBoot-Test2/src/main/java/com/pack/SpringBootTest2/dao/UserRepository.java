package com.pack.SpringBootTest2.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pack.SpringBootTest2.model.User;



public interface UserRepository extends JpaRepository<User, Integer>{
	
	List<User> findByAddress(String address);

}